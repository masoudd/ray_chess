/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef raychess_util_h
#define raychess_util_h

// char -> pointer
struct map_member {
    char name;
    void *pointer;
};
struct map {
    struct map_member *array;
    int len;
    int latest;
};
struct map *map_init(int size);
void map_free(struct map *m);
int map_set(struct map *m, char key, void *value);
void *map_get(struct map *m, char key);


int min(const int, const int);
int max(const int, const int);

#endif // raychess_util_h
