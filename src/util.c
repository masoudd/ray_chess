/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "util.h"
#include <stdlib.h>

struct map *map_init(int size) {
    struct map *r = malloc(sizeof(struct map));
    if (r == NULL) {
        return NULL;
    }

    r->len = size;
    r->latest = -1;
    r->array = malloc(sizeof(struct map_member) * size);
    if (r->array == NULL) {
        free(r);
        return NULL;
    }
    return r;
}

void map_free(struct map *m) {
    for (int i = 0; i < m->len; i++) {
        free(m->array[i].pointer);
    }
    free(m->array);
    free(m);
}

// value should be return value of malloc()
// because map_free will call free() on it
int map_set(struct map *m, char key, void *value) {
    if ((m->latest + 1) >= m->len) {
        return 1;
    }
    m->latest += 1;
    m->array[m->latest].name = key;
    m->array[m->latest].pointer = value;
    return 0;
}

void *map_get(struct map *m, char key) {
    for (int i = 0; i <= m->latest; i++) {
        if (m->array[i].name == key) {
            return m->array[i].pointer;
        }
    }
    return NULL;
}


int min(const int a, const int b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

int max(const int a, const int b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}
