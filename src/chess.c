/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

#include "chess.h"
#include "util.h"

bool has_castle_right(struct state *, char);
void remove_castle_right(struct state *, char);


const char* starting_position = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

char chess_rank_to_chess(int rank) {
    return '1' + rank;
}

char chess_file_to_chess(int file) {
    return 'a' + file;
}

int chess_chess_to_rank(char crank) {
    return crank - '1';
}

int chess_chess_to_file(char cfile) {
    return cfile - 'a';
}

int fen_to_state(const char *fen, struct state *stt) {

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            stt->board[i][j] = 0;
        }
    }
    for (int i = 0; i < 5; i++) {
        stt->castling[i] = 0;
    }
    for (int i = 0; i < 3; i++) {
        stt->enpassant[i] = 0;
    }

    // board
    for (int rank = 7, file = 0; *fen != ' '; fen++) {
        if (*fen == '/') {
            rank -= 1;
            file = 0;
            continue;
        }
        if (isdigit(*fen)) {
            file += ((*fen - '0'));
            continue;
        }
        if (isalpha(*fen)) {
            stt->board[rank][file] = *fen;
            file += 1;
            continue;
        }
    }
    while (*fen == ' ') {
        fen++;
    }

    // w or b turn
    stt->turn = *fen;
    fen++;

    while (*fen == ' ') {
        fen++;
    }

    // castling
    for (int i = 0; i < 4 && *fen != ' '; i++, fen++) {
        stt->castling[i] = *fen;
    }

    while (*fen == ' ') {
        fen++;
    }

    // enpassant
    if (*fen == '-') {
        stt->enpassant[0] = '-';
        stt->enpassant[1] = '\0';
        fen += 2;
    } else {
        stt->enpassant[0] = *fen;
        fen++;
        stt->enpassant[1] = *fen;
        fen++;
    }

    while (*fen == ' ') {
        fen++;
    }

    stt->halfmove = atoi(fen);

    while (*fen != ' ') {
        fen++;
    }

    while (*fen == ' ') {
        fen++;
    }

    stt->fullmove = atoi(fen);
    return 0;

}

bool knight_stub(struct state *stt, int rank, int file, char e_knight) {
    if (rank > 7 || rank < 0 || file < 0 || file > 7) {
        return false;
    } else {
        return stt->board[rank][file] == e_knight;
    }
}

bool bishop_valid_move(struct move *move, struct state *stt) {
    // check if it's not diagonal
    if (abs(move->file1 - move->file2) != abs(move->rank1 - move->rank2)) {
        return false;
    }

    char piece = stt->board[move->rank1][move->file1];
    char dest = stt->board[move->rank2][move->file2];
    bool inway = false;
    int rsign = (move->rank2 > move->rank1) ? +1 : -1;
    int fsign = (move->file2 > move->file1) ? +1 : -1;

    for (int r = (move->rank1 + rsign), f = (move->file1 + fsign); r != move->rank2; r += rsign, f += fsign) {
        if (stt->board[r][f]) {
            inway = true;
            break;
        }
    }
    if (!inway && (!dest || (isupper(piece) ? islower(dest) : isupper(dest)))) {
        return true;
    } else {
        return false;
    }

}

bool rook_valid_move(struct move *move, struct state *stt) {
    // rule out non vertical or horizontal moves
    if (move->file1 != move->file2 && move->rank1 != move->rank2) {
        return false;
    }

    char piece = stt->board[move->rank1][move->file1];
    char dest = stt->board[move->rank2][move->file2];
    bool inway = false;

    //vertical
    if (move->file1 == move->file2) {
        for (int r = min(move->rank1, move->rank2) + 1; r < max(move->rank1, move->rank2); r++) {
            if (stt->board[r][move->file1]) {
                inway = true;
                break;
            }
        }

    //horizontal
    } else if (move->rank1 == move->rank2) {
        for (int f = min(move->file1, move->file2) + 1; f < max(move->file1, move->file2); f++) {
            if (stt->board[move->rank1][f]) {
                inway = true;
                break;
            }
        }
    }

    if (!inway && (!dest || (isupper(piece) ? islower(dest) : isupper(dest)))) {
        return true;
    } else {
        return false;
    }
}

// check if the given king is in check
bool chess_in_check(struct state *stt, char king) {
    // can't be in check if it's not a king
    if (king != 'k' && king != 'K') {
        return false;
    }

    // find where the king is
    int king_rank = 0;
    int king_file = 0;
    bool done = false;
    for (int rank = 0; rank < 8 && !done; rank++) {
        for (int file = 0; file < 8 && !done; file++) {
            if (stt->board[rank][file] == king) {
                king_rank = rank;
                king_file = file;
                done = true;
            }
        }
    }
    if (!done) {
        // TODO did not find the king. handle the error
    }

    char e_rook = isupper(king) ? 'r' : 'R'; //enemy rook
    char e_queen = isupper(king) ? 'q' : 'Q'; //enemy queen
    char e_bishop = isupper(king) ? 'b' : 'B'; //enemy bishop
    char e_pawn = isupper(king) ? 'p' : 'P'; //enemy pawn
    char e_knight = isupper(king) ? 'n' : 'N'; //enemy knight
    char e_king = isupper(king) ? 'k' : 'K'; //enemy king
    //check for e_queen or e_rook on ranks and files
    //files
    for (int rank = (king_rank + 1); rank < 8; rank++) { //up
        char p = stt->board[rank][king_file];
        if (p == e_rook || p == e_queen) { //in check
            return true;
        } else if (p != 0) { // def not in check
            break;
        }
    }
    for (int rank = (king_rank - 1); rank >= 0; rank--) { //down
        char p = stt->board[rank][king_file];
        if (p == e_rook || p == e_queen) { //in check
            return true;
        } else if (p != 0) { // def not in check
            break;
        }
    }
    //ranks
    for (int file = (king_file + 1); file < 8; file++) { //right
        char p = stt->board[king_rank][file];
        if (p == e_rook || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }
    for (int file = (king_file - 1); file >= 0; file--) { //left
        char p = stt->board[king_rank][file];
        if (p == e_rook || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }

    //check for e_bishops and e_queens in diagonals
    // up right diag
    for (int file = (king_file + 1), rank = (king_rank + 1); file < 8 && rank < 8; file++, rank++) {
        char p = stt->board[rank][file];
        if (p == e_bishop || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }
    // up left diag
    for (int file = (king_file - 1), rank = (king_rank + 1); file >= 0 && rank < 8; file--, rank++) {
        char p = stt->board[rank][file];
        if (p == e_bishop || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }
    // down left diag
    for (int file = (king_file - 1), rank = (king_rank - 1); file >= 0 && rank >= 0; file--, rank--) {
        char p = stt->board[rank][file];
        if (p == e_bishop || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }
    // down right diag
    for (int file = (king_file + 1), rank = (king_rank - 1); file < 8 && rank >= 0; file++, rank--) {
        char p = stt->board[rank][file];
        if (p == e_bishop || p == e_queen) { //in check
            return true;
        } else if (p != 0) { //def not in check
            break;
        }
    }

    // check for e_knights
    int r = king_rank;
    int f = king_file;
    if (knight_stub(stt, r+1, f+2, e_knight) ||
        knight_stub(stt, r+2, f+1, e_knight) ||
        knight_stub(stt, r+2, f-1, e_knight) ||
        knight_stub(stt, r+1, f-2, e_knight) ||
        knight_stub(stt, r-1, f+2, e_knight) ||
        knight_stub(stt, r-2, f+1, e_knight) ||
        knight_stub(stt, r-2, f-1, e_knight) ||
        knight_stub(stt, r-1, f-2, e_knight)) {

        return true;
    }

    // check for e_king
    for (int rank = max(king_rank - 1, 0); rank <= min(king_rank + 1, 7); rank++) {
        for (int file = max(king_file - 1, 0); file <= min(king_file + 1, 7); file++) {
            if (knight_stub(stt, rank, file, e_king)) {
                return true;
            }
        }
    }

    // check for e_pawn
    int rd = isupper(king) ? +1 : -1;
    if (knight_stub(stt, r+rd, f-1, e_pawn) ||
        knight_stub(stt, r+rd, f+1, e_pawn)) {

        return true;
    }

    // not in check then
    return false;
}

bool chess_valid_move(struct move *move, struct state *stt) {
    if (move->rank1 == move->rank2 && move->file1 == move->file2) {
        return false;
    }
    char piece = stt->board[move->rank1][move->file1];
    char turn = isupper(piece) ? 'w' : 'b';
    if ((stt->turn != turn) || (!isalpha(piece))) {
        return false;
    }
    // check if player doing the move is in check after the move
    struct state state_after = *stt;
    chess_do_move(move, &state_after);
    char king = (stt->turn == 'w') ? 'K' : 'k';
    if (chess_in_check(&state_after, king)) {
        return false;
    }

    // check if move is legal
    char dest = stt->board[move->rank2][move->file2];
    //pawn
    if (piece == 'p' || piece == 'P') {
        //move
        if (move->file1 == move->file2) {
            // don't let pawns go backwards
            if (piece == (move->rank1 > move->rank2 ? 'P' : 'p')) {
                return false;
            }
            //first rank
            if (abs(move->rank1 - move->rank2) == 2 ) {
                char inway = stt->board[(move->rank2 + move->rank1)/2][move->file1];
                if (piece == 'p') { //black
                    return (move->rank1 == 6 && !dest && !inway);
                }
                if (piece == 'P') { //white
                    return (move->rank1 == 1 && !dest && !inway);
                }
            }
            // not first rank (can be promotion)
            if (abs(move->rank1 - move->rank2) == 1) {
                return !dest;
            } else {
                return false;
            }
        //capture (also can be promotion)
        } else if (abs(move->file1 - move->file2) == 1 &&
                   (move->rank2 == ((isupper(piece) ? +1 : -1) + move->rank1)) &&
                   (isupper(piece) ? islower(dest) : isupper(dest))) {
            return true;

        // enpassant
        } else if (abs(move->file1 - move->file2) == 1 &&
                    (move->rank2 == ((isupper(piece) ? +1 : -1) + move->rank1)) &&
                    stt->enpassant[0] == chess_file_to_chess(move->file2) &&
                    stt->enpassant[1] == chess_rank_to_chess(move->rank2)) {
            return true;
        // not move or capture or enpassant
        } else {
            return false;
        }

    //king
    } else if (piece == 'K' || piece == 'k') {
        if (abs(move->rank1 - move->rank2) <= 1 &&
            abs(move->file1 - move->file2) <= 1 &&
            (!dest || (isupper(piece) ? islower(dest) : isupper(dest)))) {
            return true;
        // castle
        } else if (move->file1 == 4 &&
                   move->rank1 == move->rank2 &&
                   abs(move->file1 - move->file2) == 2 &&
                   (isupper(piece) ? move->rank1 == 0 : move->rank1 == 7) &&
                   !stt->board[move->rank2][move->file2]) {
            char fen_castle = (move->file1 > move->file2) ? 'q' : 'k';
            fen_castle = isupper(piece) ? toupper(fen_castle) : tolower(fen_castle);
            struct move in_travel = *move;
            in_travel.file2 = (in_travel.file1 + in_travel.file2) / 2; // to check if king is in check on the way

            // has castling right?
            if (!has_castle_right(stt, fen_castle)) {
                return false;
            }
            // see if path of queen side rook is clear
            if ((move->file1 > move->file2) && stt->board[move->rank1][1]) {
                return false;
            }
            return chess_valid_move(&in_travel, stt);
        // not a valid move
        } else {
            return false;
        }
    //rook
    } else if (piece == 'R' || piece == 'r') {
        return rook_valid_move(move, stt);

    //bishop
    } else if (piece == 'B' || piece == 'b') {
        return bishop_valid_move(move, stt);

    //knight
    } else if (piece == 'N' || piece == 'n') {
        int rdelta = abs(move->rank1 - move->rank2);
        int fdelta = abs(move->file1 - move->file2);

        if ((!dest || (isupper(piece) ? islower(dest) : isupper(dest))) &&
            ((rdelta == 2 && fdelta == 1) || (rdelta == 1 && fdelta == 2))) {
            return true;
        } else {
            return false;
        }

    //queen
    } else if (piece == 'Q' || piece == 'q') {
        return bishop_valid_move(move, stt) || rook_valid_move(move, stt);

    } else {
        return false;
    }

}

void chess_do_move(struct move *move, struct state *stt) {

    char piece = stt->board[move->rank1][move->file1];
    stt->board[move->rank1][move->file1] = 0;
    stt->board[move->rank2][move->file2] = piece;
    stt->turn = (stt->turn == 'w') ? 'b' : 'w';
    stt->halfmove++;
    if (stt->turn == 'w') {
        stt->fullmove++;
    }

    // castle
    if (tolower(piece) == 'k' && abs(move->file1 - move->file2) == 2) {
        int rook_rank = isupper(piece) ? 0 : 7;
        int rook_file = move->file1 > move->file2 ? 0 : 7;
        stt->board[rook_rank][rook_file] = 0;
        stt->board[move->rank1][(move->file1 + move->file2) / 2] = isupper(piece) ? 'R' : 'r';
    }

    // if king moves, remove castling rights
    if (tolower(piece) == 'k') {
        if (piece == 'K') {
            remove_castle_right(stt, 'K');
            remove_castle_right(stt, 'Q');
        } else {
            remove_castle_right(stt, 'k');
            remove_castle_right(stt, 'q');
        }
    }
    // if rook moves or gets taken, remove that side (q or k) castling right
    if ((move->rank1 == 0 && move->file1 == 0) ||
        (move->rank2 == 0 && move->file2 == 0)) {

        remove_castle_right(stt, 'Q');
    } else if ((move->rank1 == 0 && move->file1 == 7) ||
               (move->rank2 == 0 && move->file2 == 7)) {

        remove_castle_right(stt, 'K');
    } else if ((move->rank1 == 7 && move->file1 == 0) ||
               (move->rank2 == 7 && move->file2 == 0)) {

        remove_castle_right(stt, 'q');
    } else if ((move->rank1 == 7 && move->file1 == 7) ||
               (move->rank2 == 7 && move->file2 == 7)) {

        remove_castle_right(stt, 'k');
    }

    // check if it is enpassant, remove the enemy pawn from board
    if (tolower(piece) == 'p' &&
        stt->enpassant[0] == chess_file_to_chess(move->file2) &&
        stt->enpassant[1] == chess_rank_to_chess(move->rank2)) {

        int f = chess_chess_to_file(stt->enpassant[0]);
        int r = chess_chess_to_rank(stt->enpassant[1]);
        if (islower(piece)) {
            r += 1;
        } else {
            r -= 1;
        }
        stt->board[r][f] = 0;
    }

    // update enpassant in state
    if (tolower(piece) == 'p' && abs(move->rank1 - move->rank2) == 2) {
        stt->enpassant[0] = chess_file_to_chess(move->file1);
        stt->enpassant[1] = chess_rank_to_chess((move->rank1 + move->rank2) / 2);
        stt->enpassant[2] = '\0';
    } else {
        stt->enpassant[0] = '-';
        stt->enpassant[1] = '\0';
    }
}

bool chess_is_promotion(struct move *move, struct state *stt) {
    char piece = stt->board[move->rank1][move->file1];
    if (piece != 'p' && piece != 'P') {
        return false;
    }

    if (move->rank2 != (isupper(piece) ? 7 : 0)) {
        return false;
    }

    return true;
}

//check if state has the castle right {k, K, q, Q}
bool has_castle_right(struct state *stt, char castle) {
    if (castle != 'k' && castle != 'K' && castle != 'q' && castle != 'Q') {
        return false;
    }
    for (int i = 0; i < 4; i++) {
        if (stt->castling[i] == castle) {
            return true;
        }
    }

    return false;
}

void remove_castle_right(struct state *stt, char castle) {
    for (int i = 0; i < 5; i++) {
        if (stt->castling[i] == castle) {
            // shift the rest to the right
            for (int j = i + 1; j < 5; j++) {
                stt->castling[j - 1] = stt->castling[j];
            }
            return;
        }
    }
    if (stt->castling[0] == '\0') {
        stt->castling[0] = '-';
        stt->castling[1] = '\0';
    }
}


//state_to_fen;
