#include <stdio.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "chess_suite/chess_test.h"

int main(int argc, char *argv[]) {

    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    CU_pSuite chess_suite = NULL;
    chess_suite = CU_add_suite("chess.c", init_chess, clean_chess);
    if (NULL == chess_suite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_ADD_TEST(chess_suite, test_chess_rank_to_chess)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
