@ECHO OFF
cd ..\src
SET RAYLIB_PATH=C:\raylib
SET COMPILER_PATH=C:\raylib\w64devkit\bin
SET PATH=%COMPILER_PATH%
SET CC=gcc
SET CFLAGS=%RAYLIB_PATH%\raylib\src\raylib.rc.data -s -static -Os -std=c99 -Wall -Wextra -Iexternal -I%RAYLIB_PATH%\raylib\src -I%RAYLIB_PATH%\raygui\src -DPLATFORM_DESKTOP -mwindows
SET LDFLAGS=-lraylib -lopengl32 -lgdi32 -lwinmm
%CC% -o ..\chess.exe main.c util.c chess.c gui.c config.c %CFLAGS% %LDFLAGS%
cd ..
IF %ERRORLEVEL% EQU 0 chess.exe
cd windows
