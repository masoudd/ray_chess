/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "raylib.h"

#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raygui.h"
#undef RAYGUI_IMPLEMENTATION

#include "gui.h"
#include "util.h"
#include "config.h"

enum gui_screen { BOARD=0, ABOUT, SETTINGS };

static enum gui_screen active_screen;
static int screenWidth;
static int screenHeight;
static int length;  // length of each cell in the 8x8 chess board

struct map *textures;
struct map *images;  //Keep these for resizing the textures on window resize.
static bool drag = false;
static int drag_x = 0;
static int drag_y = 0;
static char drag_piece;
const Color dark = GRAY;
const Color light = LIGHTGRAY;
static float min_control_width = 128;
static float control_width;
static float control_height;
static float control_x;
static float control_y;
static Color transparent;

static Vector2 _char_v;
static int font_size;

struct promotion {
    bool active;
    int file;
    bool white;
    char choice;
    bool chosen;
};
static struct promotion promotion;


// same as GuiImageButton in raygui but don't apply Fade(, guiAlpha) to the
// texture as tint
bool gui_image_button(Rectangle bounds, const char *text, Texture2D texture) {
    Rectangle texSource = (Rectangle){0, 0, (float)texture.width, (float)texture.height};

    GuiState state = guiState;
    bool clicked = false;

    // Update control
    //--------------------------------------------------------------------
    if ((state != STATE_DISABLED) && !guiLocked) {
        Vector2 mousePoint = GetMousePosition();

        // Check button state
        if (CheckCollisionPointRec(mousePoint, bounds)) {
            if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) state = STATE_PRESSED;
            else if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) clicked = true;
            else state = STATE_FOCUSED;
        }
    }
    //--------------------------------------------------------------------

    // Draw control
    //--------------------------------------------------------------------
    GuiDrawRectangle(bounds, GuiGetStyle(BUTTON, BORDER_WIDTH), Fade(GetColor(GuiGetStyle(BUTTON, BORDER + (state*3))), guiAlpha), Fade(GetColor(GuiGetStyle(BUTTON, BASE + (state*3))), guiAlpha));

    if (text != NULL) GuiDrawText(text, GetTextBounds(BUTTON, bounds), GuiGetStyle(BUTTON, TEXT_ALIGNMENT), Fade(GetColor(GuiGetStyle(BUTTON, TEXT + (state*3))), guiAlpha));
    DrawTexturePro(texture, texSource, bounds, (Vector2){0.0f, 0.0f}, 0.0f, WHITE);
//    if (texture.id > 0) DrawTextureRec(texture, texSource, RAYGUI_CLITERAL(Vector2){ bounds.x + bounds.width/2 - texSource.width/2, bounds.y + bounds.height/2 - texSource.height/2 }, WHITE);
    //------------------------------------------------------------------

    return clicked;
}

// recalculate lengths for all gui controls
// returns true if we need to resize the textures
bool resize() {
    int last_length = length;

    control_width = max(screenWidth - screenHeight, min_control_width);
    length = min(screenWidth - control_width, screenHeight) / 8;
    control_height = screenHeight;
    control_width -= 1; //add a vertical border between board and controls
    control_x = length * 8 + 1;
    control_y = 0;

    return !(last_length == length);
}

// initialize textures and scale them to length
void resize_textures() {
    if (textures != NULL) {
        for (int i = 0; i < textures->len; i++) {
            Texture2D *t = textures->array[i].pointer;
            UnloadTexture(*t);
        }
        map_free(textures);
    }

    textures = map_init(images->len);
    if (textures == NULL) {
        exit(EXIT_FAILURE);
    }

    Image tmp;
    Texture2D *tptr;
    for (int i = 0; i < images->len; i++) {
        tmp = ImageCopy(*(Image*)(images->array[i].pointer));
        ImageResize(&tmp, length, length);
        tptr = malloc(sizeof(Texture2D));
        if (tptr == NULL) {
            exit(EXIT_FAILURE);
        }

        *tptr = LoadTextureFromImage(tmp);
        map_set(textures, images->array[i].name, tptr);
        UnloadImage(tmp);
    }

}

void gui_handle_mouse(struct state *state) {
    if (promotion.active) {
        return;
    }

    int mouse_x = GetMouseX();
    int mouse_y = GetMouseY();
    if (drag) {
        if (IsMouseButtonUp(MOUSE_LEFT_BUTTON)) {
            #ifdef DEBUG
            fprintf(stderr, "gui_handle_mouse(): mouse button is up, ending drag mode\n");
            #endif
            drag = false;
            // if cursor is outside the board, do nothing
            if (mouse_x < (length * 8) && mouse_y < (length * 8) &&
                mouse_x >= 0 && mouse_y >= 0) {

                int file = (mouse_x / length);
                int rank = 7 - (mouse_y / length);
                int drag_file = drag_x;
                int drag_rank = 7 - drag_y;
                struct move *move = &(struct move) {drag_rank, drag_file,
                                                    rank, file};
                if (chess_valid_move(move, state)) {
                    bool prom = chess_is_promotion(move, state);
                    chess_do_move(move, state);
                    if (prom) {
                        promotion.active = true;
                        promotion.file = move->file2;
                        promotion.white = isupper(drag_piece);
                        promotion.choice = 0;
                        promotion.chosen = false;
                    }
                }
            }
        } else {
            Texture2D *t = map_get(textures, drag_piece);
            if (t == NULL) {
                fprintf(stderr, "Could not find piece %c in textures map\n", drag_piece);
            } else {
                #ifdef DEBUG
                fprintf(stderr, "gui_handle_mouse(): in drag mode, drawing semi transparent dragged piece\n");
                #endif
                DrawRectangle(drag_x * length, drag_y * length, length, length,
                              ((drag_x+drag_y) % 2) ? dark : light);
                DrawTextureEx(*t,
                        (Vector2){ .x = drag_x * length, .y = drag_y * length},
                        0,
                        (float) length / t->width,
                        transparent);
                DrawTextureEx(*t,
                              (Vector2){ .x = mouse_x - (length/2), .y = mouse_y - (length/2)},
                              0,
                              (float) length / t->width,
                              WHITE);

            }
        }
    }
    if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
        drag_x = mouse_x / length;
        drag_y = mouse_y / length;
        int file = drag_x;
        int rank = 7 - drag_y;
        drag = false;
        if (file < 8 && rank < 8 && file >= 0 && rank >= 0) {
            drag_piece = state->board[rank][file];
            if (isalpha(drag_piece) && ((state->turn == 'w') ? isupper(drag_piece) : islower(drag_piece))) {
                drag = true;
            }
        }
    }
    #ifdef DEBUG
    fprintf(stderr, "gui_handle_mouse(): drag: %c, drag_x: %d, drag_y: %d, mouse_x: %d, mouse_y: %d\n", drag ? 'T':'F', drag_x, drag_y, mouse_x, mouse_y);
    #endif
}

void gui_init() {
    screenWidth = atoi(config_get("screenWidth"));
    screenHeight = atoi(config_get("screenHeight"));
    font_size = atoi(config_get("font_size"));

    transparent = WHITE;
    transparent.a = 128;



    InitWindow(screenWidth, screenHeight, config_get("window_title"));
    SetWindowState(FLAG_WINDOW_RESIZABLE);
    SetTargetFPS(atoi(config_get("target_fps")));

    _char_v = MeasureTextEx(GetFontDefault(), "a", font_size, 1.0);

    const char *piece_list = config_get("piece_list");
    const char *pieces_base = config_get("pieces_base");

    textures = NULL;
    images = map_init(strlen(piece_list));
    if (images == NULL) {
        exit(EXIT_FAILURE);
    }

    char *path = malloc(strlen(pieces_base) + 10);
    if (path == NULL) {
        exit(EXIT_FAILURE);
    }

    for (const char *p = piece_list; *p != '\0'; p++) {
        path[0] = 0;
        strcat(path, pieces_base);
        strcat(path, config_get_piece(*p));

        Image *i = malloc(sizeof(Image));
        if (i == NULL) {
            exit(EXIT_FAILURE);
        }
        *i = LoadImage(path);


        if (map_set(images, *p, i) != 0) {
            exit(EXIT_FAILURE);
        }

    }
    resize();
    resize_textures();

    free(path);
}

void gui_free() {
    for (int i = 0; i < textures->len; i++) {
        Texture2D *t = textures->array[i].pointer;
        UnloadTexture(*t);
    }
    map_free(textures);

    for (int i = 0; i < images->len; i++) {
        Image *iptr = images->array[i].pointer;
        UnloadImage(*iptr);
    }
    map_free(images);

    CloseWindow();        // Close window and OpenGL context

}

void gui_check_resize() {
    static int counter;
    static bool should_resize_textures;
    if (IsWindowResized()) {
        #ifdef DEBUG
            fprintf(stderr, "GUI Resized. w: %d, h: %d\n", screenWidth, screenHeight);
        #endif
        counter = 0;
        screenWidth = GetScreenWidth();
        screenHeight = GetScreenHeight();
        should_resize_textures = resize() || should_resize_textures;
    }

    if (should_resize_textures) {
        counter += 1;
        if (counter > 30) {
            resize_textures();
            should_resize_textures = false;
        }
    }
}

void gui_draw_board() {
    for (int x = 0; x < 8; x++) {
        for (int y = 0; y < 8; y++) {
            DrawRectangle(x * length, y * length, length, length,
                    ((x+y) % 2) ? dark : light);
        }
    }
}

void gui_draw_pieces(struct state *state) {

    int file;
    int rank;
    for (int x = 0; x < 8; x++) {
        file = x;
        for (int y = 0; y < 8; y++) {
            rank = 7 - y;

            char piece = state->board[rank][file];
            if (isalpha(piece)) {
                Texture2D *t = map_get(textures, piece);
                if (t == NULL) {
                    fprintf(stderr, "Could not find piece %c in textures map\n", piece);
                } else {
                    float piece_scale = (float) length / t->width;
                    Vector2 position = { .x = x * length, .y = y * length};
                    DrawTextureEx(*t, position, 0, piece_scale, WHITE);
                }
            }
        }
    }
}

void gui_draw_controls(struct state *state) {
    if (promotion.active) {
        GuiLock();
    }
    if (GuiButton((Rectangle){control_x, control_y, control_width, length/4}, GuiIconText(ICON_PLAYER_JUMP, "New Game"))) {
        fen_to_state(starting_position, state);
    }

    if (GuiButton((Rectangle){control_x, control_y + 1*(length/4), control_width, length/4}, "Promote State")) {
        fen_to_state("kn1n1r2/pp2P3/8/8/8/6p1/PP6/KN3Q1B w - - 0 1", state);
    }
    GuiLabel((Rectangle){control_x, control_y + 2*(length/4), control_width, length/4}, (state->turn == 'w') ? "White's turn": "Black's turn");
    GuiLabel((Rectangle){control_x, control_y + 3*(length/4), control_width, length/4}, state->castling);
    GuiLabel((Rectangle){control_x, control_y + 4*(length/4), control_width, length/4}, state->enpassant);
//    gui_image_button((Rectangle) {control_x, control_y + 5*(length/4), control_width, control_width}, NULL, *(Texture2D *)(map_get(textures, 'N')));
    if (GuiButton((Rectangle){control_x, control_y + 5*(length/4), control_width, length/4}, GuiIconText(ICON_BOX_GRID, "Board"))) {
        active_screen = BOARD;
    }
    if (GuiButton((Rectangle){control_x, control_y + 6*(length/4), control_width, length/4}, GuiIconText(ICON_GEAR_BIG, "Settings"))) {
        active_screen = SETTINGS;
    }
    if (GuiButton((Rectangle){control_x, control_y + 7*(length/4), control_width, length/4}, GuiIconText(ICON_INFO, "About"))) {
        active_screen = ABOUT;
    }

    GuiUnlock();
}

void gui_draw_coordinates(void) {

    static char str[2];
    str[1] = '\0';
    Color c;
    for (int i = 0; i < 8; i++) {
        c = (i % 2) == 0 ? light : dark;
        str[0] = i + 'a';
        DrawText(str, (i * length) + (font_size/8), (8 * length) - _char_v.y, font_size, c);

        str[0] = '8' - i;
        DrawText(str, (8 * length) - _char_v.x - (font_size/8), (i * length), font_size, c);
    }
}

void gui_draw_promotion(struct state *state) {
    if (promotion.chosen) {
        state->board[promotion.white ? 7 : 0][promotion.file] = promotion.choice;
        promotion.chosen = false;
        return;
    }

    if (!promotion.active) {
        return;
    }


    float start_x = length * 2;
    float start_y = (length / 2.0f) + (promotion.white ? 0 : 6) * length;
    // fade everything else
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), Fade(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)), 0.85f));
    if (gui_image_button((Rectangle){start_x + (0 * length), start_y, length, length}, NULL, *(Texture2D *)map_get(textures, promotion.white ? 'Q' : 'q'))) {
        //promote to queen
        promotion.active = false;
        promotion.choice = promotion.white ? 'Q' : 'q';
        promotion.chosen = true;
    }
    if (gui_image_button((Rectangle){start_x + (1 * length), start_y, length, length}, NULL, *(Texture2D *)map_get(textures, promotion.white ? 'R' : 'r'))) {
        //promote to rook
        promotion.active = false;
        promotion.choice = promotion.white ? 'R' : 'r';
        promotion.chosen = true;
    }
    if (gui_image_button((Rectangle){start_x + (2 * length), start_y, length, length}, NULL, *(Texture2D *)map_get(textures, promotion.white ? 'B' : 'b'))) {
        //promote to bishop
        promotion.active = false;
        promotion.choice = promotion.white ? 'B' : 'b';
        promotion.chosen = true;
    }
    if (gui_image_button((Rectangle){start_x + (3 * length), start_y, length, length}, NULL, *(Texture2D *)map_get(textures, promotion.white ? 'N' : 'n'))) {
        //promote to knight
        promotion.active = false;
        promotion.choice = promotion.white ? 'N' : 'n';
        promotion.chosen = true;
    }
}

void draw_settings(void) {
    DrawText("Settings page", length*4, length*4, length/4, BLACK);
}

void gui_draw_screen(struct state *state) {
    switch(active_screen) {
        case BOARD:
            gui_draw_board();
            gui_draw_pieces(state);
            gui_draw_coordinates();
            gui_draw_controls(state);
            gui_draw_promotion(state);
            gui_handle_mouse(state);
        break;

        case ABOUT:
            gui_draw_controls(state);
            DrawText("About page", length*4, length*4, length/4, BLACK);
            DrawText("Chess by masoudd", length*4, length*4 + length/4, length/4, BLACK);
            DrawText("raylib version: "RAYLIB_VERSION, length*4, length*4 + 2*length/4, length/4, BLACK);
            DrawText("raygui version: "RAYGUI_VERSION, length*4, length*4 + 3*length/4, length/4, BLACK);
        break;

        case SETTINGS:
            gui_draw_controls(state);
            draw_settings();
        break;
    }
}
