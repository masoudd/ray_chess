#!/bin/bash

shopt -s failglob

width="${1:-200}"
for name in *.svg;
    do rsvg-convert "${name}" --keep-aspect-ratio --width ${width} --output "${name%svg}png";
done
