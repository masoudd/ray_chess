A desktop gui chess client written with [raylib](https://raylib.com)

TODO:
  - Add chess engine interface (Universal Chess Interface or UCI)
  - Add settings menues

Screenshots:

![Starting](https://codeberg.org/masoudd/ray_chess/raw/branch/master/screenshots/starting.png)
