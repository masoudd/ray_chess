/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <ctype.h>

#include "config.h"

//const char* pieces_base = "chesspieces/wikipedia/";
const char* pieces_base = "chesspieces/staunty/";
//const char* pieces_base = "chesspieces/cburnett/";
/*
const char* pieces[12] = {"bR.png", "bN.png", "bB.png", "bQ.png", "bK.png", "bP.png",
                          "wR.png", "wN.png", "wB.png", "wQ.png", "wK.png", "wP.png"};

*/

const char *config_get_piece(char symbol) {
    static char piece[] = "bR.png";
    if (isupper(symbol)) {
        piece[0] = 'w';
    } else {
        piece[0] = 'b';
    }
    piece[1] = toupper(symbol);
    return piece;
}

const char *config_get(const char *name) {
    if (!strcmp(name, "screenWidth")) {
        return "1000";

    } else if (!strcmp(name, "screenHeight")) {
        return "800";

    } else if (!strcmp(name, "pieces_base")) {
        return pieces_base;

    } else if (!strcmp(name, "piece_list")) {
        return "rbnqkpRBNQKP";

    } else if (!strcmp(name, "window_title")) {
        return "raychess";

    } else if (!strcmp(name, "target_fps")) {
        return "60";
    } else if (!strcmp(name, "font_size")) {
        return "20";
    }
    return NULL;
}

