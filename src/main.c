/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "raylib.h"

#include "chess.h"
#include "util.h"
#include "gui.h"
#include "config.h"

// Board state
struct state state;


void init() {
    #ifdef DEBUG
        fprintf(stderr, "Running in debug mode, setting trace log level to LOG_DEBUG\n");
        SetTraceLogLevel(LOG_DEBUG);
    #else
        SetTraceLogLevel(LOG_WARNING);
    #endif
    gui_init();
    fen_to_state(starting_position, &state);
}

void cleanEverything() {
    gui_free();
}

int main(void) {

    init();
    SetExitKey(KEY_NULL);

    while (!WindowShouldClose()) {   // Detect window close button
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        BeginDrawing();

        ClearBackground(RAYWHITE);

        gui_check_resize();
        gui_draw_screen(&state);


        EndDrawing();
    }

    cleanEverything();

    return 0;
}
