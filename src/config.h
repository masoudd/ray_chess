/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef raychess_config_h
#define raychess_config_h

const char *config_get_piece(char symbol);
const char *config_get(const char *name);

#endif // raychess_config_h
