/*
    This file is part of rayChess.

    rayChess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rayChess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rayChess.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef raychess_chess_h
#define raychess_chess_h

#include <stdbool.h>

extern const char* starting_position;

char chess_rank_to_chess(int rank);
char chess_file_to_chess(int file);
int chess_chess_to_rank(char crank);
int chess_chess_to_file(char cfile);

struct state {
    char board[8][8];

    // whether it is white's turn or black 'w' or 'b'
    char turn;

    char castling[5];
    char enpassant[3];

    //This is the number of halfmoves since the last capture or pawn advance. The reason for this field is that the value is used in the fifty-move rule.
    int halfmove;

    //The number of the full move. It starts at 1, and is incremented after Black's move.
    int fullmove;
};

struct move {
    //from
    int rank1;
    int file1;
    //to
    int rank2;
    int file2;
};

int fen_to_state(const char *fen, struct state *stt);

bool chess_valid_move(struct move *move, struct state *stt);
void chess_do_move(struct move *move, struct state *stt);
bool chess_is_promotion(struct move *move, struct state *stt);

#endif // raychess_chess_h
